import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HERMITAGE',
      theme: ThemeData(primaryColor: Colors.cyan[900]),
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  TextEditingController inputcontroller = new TextEditingController();
  static String getvalue = "";
  String greeting = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HERMITAGE'),
        backgroundColor: const Color(0xFF10323A),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.all(120.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              TextField(
                controller: inputcontroller,
                decoration: InputDecoration(
                  icon: Icon(Icons.arrow_right),
                  border: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  labelText: "Hello, what's your name?",
                  hintText: "Enter your name...",
                ),
              ),
              const SizedBox(height: 10),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    getvalue = inputcontroller.text;
                    greeting = "Welcome ";
                  });
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => DetailScreen(),
                    ),
                  );
                },
                child: const Text('OK'),
                style: ElevatedButton.styleFrom(primary: Color(0xFF020215)),
              ),
              const SizedBox(height: 10),
              Text('$greeting $getvalue'),
            ],
          ),
        ),
      ),
    );
  }
}

class Page extends StatefulWidget {
  @override
  HomePage createState() => HomePage();
}

class HomePage extends State<Page> {
  Widget build(BuildContext context) {
    return Scaffold();
  }
}

class DetailScreen extends StatelessWidget {
  @override
  String prin = HomeState.getvalue;
  Widget build(BuildContext context) {
    // Use the Todo to create the UI.
    return Scaffold(
      appBar: AppBar(
        title: Text('HERMITAGE'),
        backgroundColor: const Color(0xFF10323A),
      ),
      body: const Padding(
        padding: EdgeInsets.all(16.0),
        child: Text('WELCOME!'),
      ),
    );
  }
}

// saved by https://www.youtube.com/watch?v=q0swSlZehLQ
// tested