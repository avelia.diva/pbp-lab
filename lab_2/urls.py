from django.urls import path
from .views import index, xml, json

# Rute untuk lab-2/ index, xml, dan json

urlpatterns = [
    path('', index, name='index'),
    path('xml', xml, name='xml'),
    path('json', json, name='json'),
]