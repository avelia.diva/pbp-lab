from django.db import models


class Note(models.Model):
    to = models.CharField(max_length=30)
    by = models.CharField(max_length=30)
    title = models.CharField(max_length=30)
    message = models.TextField(max_length=5000)

# atribut: `to`, `from`, `title`, and `message`.
# Lab is tested and DONE!