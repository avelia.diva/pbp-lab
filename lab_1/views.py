# note to self : THIS BUILD IS TESTED AND DONE
 
from django.shortcuts import render
from datetime import datetime, date

# new, import new data for friend_list
from lab_1.models import Friend

mhs_name = 'Avelia Diva Zahra'  # TODO Implement this [DONE!]
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2002, 5, 12)  # TODO Implement this, format (Year, Month, Date) [DONE!]
npm = 2006596176  # TODO Implement this [DONE!]


def index(request):
    response = {'name': mhs_name,
                'age': calculate_age(birth_date.year),
                'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0


def friend_list(request):
    friends = Friend.objects.all().values()  # TODO Implement this -- implement friend di models? [DONE!]
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)
