#from lab_1.models import Friend
from django import forms
from lab_1.models import Friend

class FriendForm(forms.ModelForm):
	class Meta:
		model = Friend
		fields = "__all__"  #['display_name']
	error_messages = {
		'required' : 'Please Type'
	}
