Kemudian juga ada beberapa pertanyaan singkat yang perlu dijawab dalam file `lab_answer/lab_2.md`, yaitu:

# JAWABAN

1. Apakah perbedaan antara JSON dan XML?
- XML termasuk markup language, sementara JSON adalah notasi object dari javascript
- Bentuk kodenya berbeda. Kode JSON yang bentuknya mirip dictionary/map cenderung lebih singkat daripada XML yang berbentuk tree. JSON tidak punya tag, XML punya
- JSON Hanya mendukung string, angka, array boolean, dan objek sementara XML dapat menyimpan gambar dan grafik
- Bisa menggunakan array dengan JSON, sementara penggunaan array tidak didukung oleh XML
- Bisa menyisipkan comment di XML, sementara di JSON tidak bisa
- Orientasinya berbeda. JSON data, sementara XML dokumen.

2. Apakah perbedaan antara HTML dan XML?
- HTML berfokus pada format dan tampilan data, sementara XML berfokus kepada struktur dan konteks. HTML menunjukkan, XML mengangkut
- Tagnya. HTML menunjukkan format tampilan sementara XML memuat informasi, seperti variabelnya. Hal ini juga menyebabkan tag HTML terbatas dan tag XML tidak terbatas. Karena sudah tersedia dari awal juga, tag HTML sudah ditentukan dari awal sementara tag XML belum ditentukan dari awal.
- Untuk menampilkan informasi dengan akurat, XML case sensitif. Berbeda dengan HTML yang case-insensitive
- Walaupun singkatannya sama-sama markup langage, HTML berarti Hypertext sementara XML eXtensible.